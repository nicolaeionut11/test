const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/update-product/:id',(req,res)=>{
    if(req.params.id){
        for(let i = 0 ;i < products.length ; i ++){
                if(req.params.id == products[i].id ){
                products[i].productName = req.body.productName;
                products[i].price = req.body.price;
                res.status(200).send('Product updated successfully');
            }
        }
    }
     else{  
         res.status(500).send('errorr');
     }
});

app.delete('/delete-product',(req,res)=>{
    if(req.body.productName){
        for(let i = 0 ; i < products.length; i++){
            if(req.body.productName == products[i].productName){
                products.splice(i,1);
                
            }
        }
        res.status(200).send('Product deleted successfully');
    }
    else{
        res.status(500).send('eroare');
        }
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});