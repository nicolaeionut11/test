import React from 'react';
import ProductList from './ProductList.js'



class App extends React.Component {
  render() {
    return (
      <div className="App">
        <ProductList/>
      </div>
    );
  }
}

export default App;
