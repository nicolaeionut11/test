import React from 'react';


class ProductAddForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            price : 0,
            productName: ''
        }
    }
        handleChangePrice = (e) => {
        this.setState({
           price: e.target.value
        });
        
      }
    
       handleChangeProductName = (e) => {
        this.setState({
            productName: e.target.value
        });
        
    }
  render() {
    return (
      <div>
        <form>
            <lable for="price"> Price</lable>
            <input type="number" id="title" name="title" onChange={this.handleChangePrice}/>
       
            <lable for="productName">Product Name</lable>
            <input type="text" id="confirm" name="content" onChange={this.handleChangeProductName}/>
            <input type ="button" value="add" onClick={() => this.props.onAdd({
                price : this.state.price,
                productName : this.state.productName
            })}/>
        </form>
      </div>
    );
  }
}

export default ProductAddForm;
