import React from 'react';
import ProductStore from './ProductStore'
import ProductAddForm from './ProductAddForm'

class ProductList extends React.Component {
  constructor(){
    super()
    this.state={
      products : []
    }
    this.store = new ProductStore()
    this.add = (product) => {
        this.store.addOne(product)
    }
  }
  
  componentDidMount(){
    this.store.getAll()
    this.store.emitter.addListener('GET_ALL_SUCCSES',()=>{
      this.setState({
        products : this.store.content
      })
    })
  }
  render() {
    return (
      <div>
          <div>
               {
                this.state.products.map((e,i)=> <div key={i}>
                {e.price} {e.productName}
                </div>
                )
               }
          </div>
          <ProductAddForm onAdd={this.add}/>
      </div>
    );
  }
}

export default ProductList;
